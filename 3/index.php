<?php
// Variables
$var = 'Привет Мир!';

// Constants
define("PAGE", "New page")

// Another way to define constants
//Doesn't work in PHP 7
//const MYCONST = 'This is my const';
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $var?></title>
</head>
<body>
    <h1><?php echo $var; ?></h1>
    <h2><?php echo PAGE; ?></h2>
</body>
</html>